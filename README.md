# Ansible Role : Slurm

An Ansible role to deploy the Slurm Workload Manager :
 - Accounting
 - Controller
 - Nodes
 - Clients

## Compatibility

This role is compatible with the following environments :
* Centos 7
* Ubuntu 16.XX
* Ubuntu 18.XX
* Debian 9

## Ansible invocation

If you mount personal directories from shared storage with root squash, you should prepend your ansible-playbook
command with the following, so that root can access script files after transfer by Ansible and subsequent sudo:
ANSIBLE_REMOTE_TEMP=/tmp

This role relies on having access to all nodes' facts so it can reliably generate cluster-wide configuration.
Since on large setups a few nodes can be expected to be down at any time, it is advised to use a cache plugin.
You can prepend ANSIBLE_CACHE_PLUGIN=jsonfile to your ansible command. Remember to tell Ansible where to put
cache files in .ansible.cfg or in the global configuration :
fact_caching_connection=/tmp
Also, the default timeout for cache data is 86400 (one day) I advise setting this way higher (1 month, e.g.
ANSIBLE_CACHE_PLUGIN_TIMEOUT=2592000) so that a failing node that is awaiting repairs won't block deployments.
The cache plugin can also be specified in the config file with 'fact_caching = jsonfile' so as the timeout
(fact_caching_timeout = xxxx.)

## Role Variables

See defaults/main.yml, along the explanation below.

Available variables are listed below, along with default values (see defaults/main.yml):

| Variable                 | Description                                                                         |
|--------------------------|-------------------------------------------------------------------------------------|
| slurm_cluster_name       | The name of the SLURM cluster                                                       |
| slurm_accounting_group   | Name(s) of the group(s) containing SLURM accounting servers (see 'Groups' below)    |
| slurm_controller_group   | Name(s) of the group(s) containing SLURM controllers                                |
| slurm_client_group       | Name(s) of the group(s) containing all client machines                              |
| slurm_node_group         | Name(s) of the group(s) containing all compute nodes                                |
| mysql_root_username      | Root username for the mysql database                                                |
| mysql_root_password      | Root password for the mysql database                                                |
| mysql_enabled_on_startup | Should be yes                                                                       |
| mysql_dabatases          | List of mysql databases to create (you should at least create one for slurm)        |
| mysql_users              | List of user to create with access rights (see https://github.com/geerlingguy/ansible-role-mysql#role-variables) |
| munge_key                | Secret key used by munge. It should be the same on all SLURM components.            |

## Groups

You will need to create several groups in your inventory : four groups, one for each kind of machine (accounting,
controllers, clients and nodes). Note that these groups can contain machines from different clusters if you have
more than one. Additionnally, you should have one group per cluster so "slurm_cluster_name" can easily be
defined for each node in each cluster (either in the inventory or group_vars), but it is not mandatory. You
could also define it in the play vars if you run the role only on one cluster at a time.

## Facts

Some values in the configuration should be templated from your directives (e.g. slurm_cluster_name or
slurm_partitions), while others should be taken from the installed base. This is the case for SlurmctldPidFile
and some other file locations (log files) which can be altered by package upgrades. These values are dependent
on how the packages have been built and/or upstream defaults, so to avoid tracking these values in the role,
an ansible fact generator is used to gather them. If you intend to use the check-diff
mode on an installed cluster, make sure the facter is installed by running the role in normal mode
with "--tags facter".

This mechanism is also used to ensure that the same munge key is present on all the nodes of a cluster. If
two or more different keys are found, the role will panic. If one key is found, it will be deployed to all
other nodes. If no keys are found, one will be randomly generated and deployed. You can override this with
the "munge_key" parameter.

In order gain execution time, when using caching, care has been taken to not reload these local (but also
other regular) facts all the time. Scheduling when is the right time to reload is tricky, therefore the
facter module uses a version number and some fields are checked for sanity. Coherency checks are also made
to help the role developer to not make mistakes when increasing the version number is necessary, however
I can't guarantee that I got everything right.

## Dependencies

* mysql

## Example Playbook

```yaml
---

---

- name: High Performance Computing - Database for accounting
  become: yes
  become_method: sudo
  hosts:
    - slurm-accounting
  roles:
    - geerlingguy.mysql

- name: High Performance Computing - Slurm
  hosts:
    - slurm-controller
    - slurm-accounting
    - slurm-nodes
    - slurm-clients
  roles:
    - ifb-elixirfr.slurm
```

Inside `vars/main.yml`:

```yaml
---
mysql_root_username: root
mysql_root_password: 'thisissecret'
mysql_enabled_on_startup: yes
mysql_databases:
  - name: slurm_accounting
mysql_users:
  - name: slurm
    host: localhost
    password: 'thisissecret'
    priv: slurm_accounting.*:ALL
    append_privs: yes

slurm_node_memspeclimit: 4000
slurm_node_corespeccount: 2

slurm_partitions:
  - name: main
    nodes: slurm-node1-dev,slurm-node2-dev
    default: YES
    max_time: 0-2:00:00
    allow_accounts: ALL
    state: UP
```

## License

license (GNU 3.0)

## Author Information

This role was created in 2019 by the IFB.

